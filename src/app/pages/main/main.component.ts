import * as moment from 'moment';
import * as _ from 'lodash';
import { _ParseAST } from '@angular/compiler';
import { Component, OnInit, AfterViewInit, HostListener, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { SpeakerService } from '../../services/api/speaker.service';
import { BoothService } from '../../services/api/booth.service';
import { CookieService } from 'ngx-cookie-service';
import { environment } from '../../../environments/environment';

declare var $: any;

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('policyAlertBtn') policyAlertBtn: ElementRef;
  public speakers: Array<any> = [];
  public sponsors: Array<any> = []; // 스폰서 목록
  public selectedBooth: any;
  public mobile: boolean;

  public programBookUrl = '';
  public excerptUrl = '';

  public user;
  private koreaTimer;
  public koreaTime;

  public attachments = []; // 부스 첨부파일

  constructor(
    private speakerService: SpeakerService,
    private boothService: BoothService,
    private cookieService: CookieService,
  ) { }


  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    if (window.innerWidth < 768 !== this.mobile) {
      this.loadSpeakers();
    }
  }

  /**
   * 배열 나누기
   * @param array 배열
   * @param n n개씩 자르기
   */
  division = (array, n) => {
    return [].concat.apply([],
      array.map((item, i) => {
        return i % n ? [] : [array.slice(i, i + n)]
      })
    );
  }

  ngOnInit(): void {
    this.user = localStorage.getItem('cfair');
    this.loadSpeakers();
    this.getTime();
  }

  ngAfterViewInit(): void {
    const user = JSON.parse(localStorage.getItem('cfair'));
    if (user) {
      setTimeout(() => {
        this.policyAlertBtn.nativeElement.click();
      }, 500);
    }
  }

  ngOnDestroy(): void {
    clearInterval(this.koreaTimer);
  }

  // 발표자 리스트를 조회한다.
  loadSpeakers(): void {
    let limit = 6;
    this.mobile = window.innerWidth < 768;
    if (this.mobile) { limit = 6; }
    this.speakerService.find(false).subscribe(res => {
      this.speakers = this.division(res, limit);
    });
  }

  getTime(): void {
    this.koreaTimer = setInterval(() => {
      this.koreaTime = moment().utc().utcOffset('+0900'); // 한국 시간 : UTC +0900
    }, 1000);
  }

}
