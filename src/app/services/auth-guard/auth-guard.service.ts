import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let auth = JSON.parse(localStorage.getItem('cfair'));
    if (auth && auth.token) {
      if (route.routeConfig.path === 'login') {
        this.router.navigate(['/main']);
      }
      return true;
    } else {
      // 아래 path는 로그인 상태에서만 접근 가능
      let path = ['live', 'vod', 'vod/:vodId', 'posters', 'posters/:posterId'];

      // console.log(route.routeConfig.path)
      let result = path.indexOf(route.routeConfig.path);
      if (result > -1) {
        alert('Login is required.');
        this.router.navigate(['/login']);
      }
      return true;

    }
  }
}

